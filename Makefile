SRC_FOLDER := src/

WORKING_DIRS := $(sort $(dir $(filter-out $(wildcard $(SRC_FOLDER)*/*_test.cpp), $(wildcard $(SRC_FOLDER)*/p*.cpp))))
MISSING_DIRS := $(sort $(filter-out $(dir $(WORKING_DIRS)), $(wildcard $(SRC_FOLDER)*/)))

MAKE_LESS_DIRS := $(sort $(filter-out $(dir $(wildcard $(SRC_FOLDER)*/Makefile)), $(wildcard $(SRC_FOLDER)*/)))
CMAKE_LESS_DIRS := $(sort $(filter-out $(dir $(wildcard $(SRC_FOLDER)*/CMakeLists.txt)), $(wildcard $(SRC_FOLDER)*/)))

BASE_DIR := base_files/
BASE_MAKEFILE := $(BASE_DIR)Makefile
BASE_CMAKEFILE := $(BASE_DIR)CMakeLists.txt

PIPFILE := Pipfile
PIPFILE_LOCK := Pipfile.lock

ALL_FILES := $(sort $(wildcard $(SRC_FOLDER)*/*.h) $(wildcard $(SRC_FOLDER)*/*.cpp))

ALL_INSTALLATIONS := $(sort $(dir $(wildcard $(SRC_FOLDER)*/*.lock)))

.PHONY: default_target
default_target: help

.PHONY: list
list:
	@for a_route in $(MISSING_DIRS) ; do \
		echo $$a_route ; \
	done

.PHONY: test_targets
test_targets:
	@for a_route in $(WORKING_DIRS) ; do \
		$(MAKE) -s -C $$a_route run || exit 1 ; \
	done

.PHONY: clean
clean:
	rm -r $(SRC_FOLDER)*/{build/,Makefile,CMakeLists.txt}

.PHONY: uninstall
uninstall:
	@pipenv --rm
	@rm Pipfile.lock

$(PIPFILE_LOCK): $(PIPFILE)
	@pipenv install --dev

.PHONY: lint
lint: $(PIPFILE_LOCK)
	@pipenv run cpplint --filter=-build/include_subdir $(ALL_FILES)

.PHONY: %_lint
%_lint: $(PIPFILE_LOCK)
	@pipenv run cpplint --filter=-build/include_subdir \
    $(sort $(wildcard src/$(@:_lint=)/*.h)) \
    $(sort $(wildcard src/$(@:_lint=)/*.cpp))

.PHONY: test
test: test_targets lint

.PHONY: create_base_files
create_base_files:
	@for a_route in $(MAKE_LESS_DIRS) ; do \
		cp $(BASE_MAKEFILE) $$a_route ; \
	done
	@for a_route in $(CMAKE_LESS_DIRS) ; do \
		cp $(BASE_CMAKEFILE) $$a_route ; \
	done

.PHONY: help
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "     help (the default if no target is provided)"
	@echo "         - shows this message"
	@echo "     list"
	@echo "         - shows all the problems 'pWXYZ.h' missing an 'pWXYZ.cpp'"
	@echo "     test_targets"
	@echo "         - runs 'make run' in all implemented problems"
	@echo "     clean"
	@echo "         - deletes all 'build' folders, Makefile and CMakeLists.txt files"
	@echo "     uninstall"
	@echo "         - runs 'pipenv --rm' in all problems with a 'Pipfile.lock'"
	@echo "     lint"
	@echo "         - runs lint test in all *.{.h,.cpp} files"
	@echo "     test"
	@echo "         - runs 'test_targets' and 'lint'"
	@echo "     create_base_files"
	@echo "         - copies all the files in  'basefiles/' to each problem"
