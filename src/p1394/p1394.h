/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P1394_P1394_H_
#define SRC_P1394_P1394_H_

namespace an_inductively_defined_function {

    int f_n(const int &n);

}  // an_inductively_defined_function

#endif  // SRC_P1394_P1394_H_
