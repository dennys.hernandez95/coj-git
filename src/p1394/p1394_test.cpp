/* Copyright (c) 2018 Kevin Villalobos */
#include "p1394.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = an_inductively_defined_function;

BOOST_AUTO_TEST_CASE(case_2,
    *utf::timeout(4)) {
    const int result = tools::f_n(2);

    BOOST_REQUIRE_EQUAL(1, result);
}

BOOST_AUTO_TEST_CASE(case_53,
    *utf::timeout(4)) {
    const int result = tools::f_n(53);

    BOOST_REQUIRE_EQUAL(27, result);
}

BOOST_AUTO_TEST_CASE(case_153,
    *utf::timeout(4)) {
    const int result = tools::f_n(153);

    BOOST_REQUIRE_EQUAL(77, result);
}

BOOST_AUTO_TEST_CASE(case_coj,
    *utf::timeout(4)) {
    const int result_1 = tools::f_n(2);

    BOOST_REQUIRE_EQUAL(1, result_1);

    const int result_2 = tools::f_n(53);

    BOOST_REQUIRE_EQUAL(27, result_2);

    const int result_3 = tools::f_n(153);

    BOOST_REQUIRE_EQUAL(77, result_3);
}
