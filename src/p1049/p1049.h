/* Copyright (c) 2018 Kevin Villallobos */
#ifndef SRC_P1049_P1049_H_
#define SRC_P1049_P1049_H_

namespace sum {

    int triangular_ish(const int &N);

}  // namespace sum

#endif  // SRC_P1049_P1049_H_
