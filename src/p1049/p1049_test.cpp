/* Copyright (c) 2018 Kevin Villalobos */
#include "p1049.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = sum;

BOOST_AUTO_TEST_CASE(case_positive,
    *utf::timeout(4)) {
    const int result = tools::triangular_ish(3);

    BOOST_REQUIRE_EQUAL(6, result);
}

BOOST_AUTO_TEST_CASE(case_zero,
    *utf::timeout(4)) {
    const int result = tools::triangular_ish(0);

    BOOST_REQUIRE_EQUAL(1, result);
}

BOOST_AUTO_TEST_CASE(case_negative,
    *utf::timeout(4)) {
    const int result = tools::triangular_ish(-5);

    BOOST_REQUIRE_EQUAL(-14, result);
}
