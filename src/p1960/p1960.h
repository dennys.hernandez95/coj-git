/* Copyright (c) 2018 Kevin Villalbos */
#ifndef SRC_P1960_P1960_H_
#define SRC_P1960_P1960_H_

#include <tuple>
#include <vector>


namespace connect_the_cows {

    int routes(const std::vector< std::tuple<int, int> > &cows);

}

#endif  // SRC_P1960_P1960_H_
