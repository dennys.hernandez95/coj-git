/* Copyright (c) 2018 Kevin Villalobos*/
#ifndef SRC_P3599_P3599_H_
#define SRC_P3599_P3599_H_

#include <string>
#include <utility>


namespace complementary_words {
class complementary_words;
}

class complementary_words::complementary_words {
 private:
    std::string m_s;

 public:
    explicit complementary_words(const std::string &S);

    bool is_complementary(const std::pair<unsigned int, unsigned int> query);
};

#endif  // SRC_P3599_P3599_H_
