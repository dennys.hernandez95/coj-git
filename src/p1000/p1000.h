/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P1000_P1000_H_
#define SRC_P1000_P1000_H_

namespace a_plus_b_problem {

    int sum(const int &a, const int &b);

}

#endif  // SRC_P1000_P1000_H_
