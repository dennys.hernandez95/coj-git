/* Copyright (c) 2018 Kevin Villalobos */
#include "p1000.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = a_plus_b_problem;

BOOST_AUTO_TEST_CASE(both_positive,
    *utf::timeout(2)) {
    const int result = tools::sum(4, 5);

    BOOST_REQUIRE_EQUAL(9, result);
}

BOOST_AUTO_TEST_CASE(one_zero,
    *utf::timeout(2)) {
    const int result = tools::sum(0, 3);

    BOOST_REQUIRE_EQUAL(3, result);
}

BOOST_AUTO_TEST_CASE(both_zero,
    *utf::timeout(2)) {
    const int result = tools::sum(0, 0);

    BOOST_REQUIRE_EQUAL(0, result);
}
