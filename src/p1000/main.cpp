/* Copyright (c) 2018 Kevin Villalobos */
#include "p1000.h"

namespace tools = a_plus_b_problem;

int main() {
    int a, b;
    std::cin >> a >> b;

    std::cout << tools::sum(a, b);

    return 0;
}
