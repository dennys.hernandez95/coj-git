/* Copyright (c) 2018 Kevin Villalobos */
#include "p3283.h"
#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = alice_and_array_in_flames;

BOOST_AUTO_TEST_CASE(case_coj,
    *utf::timeout(1)) {
    const std::vector<int> integers = {
        2, 4, -5, 7, 9, 9,
    };

    tools::alice_and_array_in_flames s_tree(integers);

    const std::pair<int, int> query_1(1, 3);
    const int result_1 = s_tree.operate(tools::ASK, query_1);
    BOOST_REQUIRE_EQUAL(0, result_1);

    const std::pair<int, int> query_2(3, 5);
    s_tree.operate(tools::SET, query_2);

    const std::pair<int, int> query_3(1, 3);
    const int result_2 = s_tree.operate(tools::ASK, query_3);
    BOOST_REQUIRE_EQUAL(2, result_2);

    const std::pair<int, int> query_4(1, 6);
    const int result_3 = s_tree.operate(tools::ASK, query_4);
    BOOST_REQUIRE_EQUAL(1, result_3);

    const std::pair<int, int> query_5(4, 9);
    s_tree.operate(tools::SET, query_5);

    const std::pair<int, int> query_6(4, 6);
    const int result_4 = s_tree.operate(tools::ASK, query_6);
    BOOST_REQUIRE_EQUAL(1, result_4);

    const std::pair<int, int> query_7(4, 4);
    const int result_5 = s_tree.operate(tools::ASK, query_7);
    BOOST_REQUIRE_EQUAL(2, result_5);
}
