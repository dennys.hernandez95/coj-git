# 3283 - A+B Problem

## Description

Alice is taking a new course named
"Analysis and Design of Algorithms" as part of her Engineering degree.
This week, she and her classmates are studying about Sorting Algorithms
and their performance.
They learn about how some algorithms are very efficient under certain circumstances
and inefficient in other cases.
For instance, some of the algorithms are quite fast when sorting arrays
which are randomly ordered,
but they are remarkably slow when the input array of elements is initially sorted
or partially sorted, etc.

As part of the final project,
Alice and their classmates are working on a new algorithm to sort integers.
They are very excited by its preliminary results but not all is going well.
After many tests,
they determined that the algorithm is very inefficient when sorting sequences
that are either increasing or non-decreasing.
A sequence of numbers is said to be non-decreasing
if every next element to the right isn't less than previous element.
A sequence of numbers is said to be increasing if every next element
to the right is strictly greater than previous element.
A sequence containing a single element is said to be increasing
and non-decreasing, at the same time.

In order to prove that their suspicion is correct,
they are preparing a special array of N integer numbers.
The elements in the array are conveniently numbered 1 to N from left to right.
They need the (final) array to satisfy certain properties about the number
of increasing and non-decreasing sequences on it.
During their tests, they need to perform a sequence of operations over the array,
where each operation is one of these two types:

* Type 1: Given two integers i and x, set the value of the i-th element in the array to x.
* Type 2: Given two integers i and j, determine whether the sequence in the interval \[i, j\] (i.e. the sequence starting with the element i and ending at the element j) is an increasing sequence, non-decreasing sequence, or none of these.

As you are a smart boy, Alice is certain that you can help her with this difficult task.
Are you ready to help her?

## Input specification

The first line of input contains two space separated integers
N (1 <= N <= 10^5) and Q (1 <= Q <= 10^4).
The next line contains N space separated integers:
these are the initial values of the array listed from left to right.
The next Q lines contain three space separated integers with one of the following formats:

* 1 i x - denoting the first type of operation, with (1 <= i <= N) and x (-10^6 <= x <= 10^6).
* 2 i j - denoting the second type of operation, with (1 <= i <= j <= N).

## Output specification

For each operation of type two, output a line with an integer number between 0 and 2:

* Output 2 if the elements in the interval [i, j] form an increasing sequence.
* Else, output 1 if the elements in the interval [i, j] form non-decreasing sequence.
* Else, output 0 if the sequence formed by the elements in the interval [i, j] is neither increasing nor non-decreasing.

## Sample input

```
6 7
2 4 -5 7 9 9
2 1 3
1 3 5
2 1 3
2 1 6
1 4 9
2 4 6
2 4 4
```

## Sample output

```
0
2
1
1
2
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[3379](http://coj.uci.cu/24h/problem.xhtml?pid=3379) |
[3954](http://coj.uci.cu/24h/problem.xhtml?pid=3954) |
[2756](http://coj.uci.cu/24h/problem.xhtml?pid=2756) |
[3734](http://coj.uci.cu/24h/problem.xhtml?pid=3734) |
[2590](http://coj.uci.cu/24h/problem.xhtml?pid=2590) |
[3745](http://coj.uci.cu/24h/problem.xhtml?pid=3745)

## Source

[Alice and Array in Flames](http://coj.uci.cu/24h/problem.xhtml?pid=3283)
