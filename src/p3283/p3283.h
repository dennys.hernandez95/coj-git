/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P3283_P3283_H_
#define SRC_P3283_P3283_H_

#include <utility>
#include <vector>

namespace alice_and_array_in_flames {

class alice_and_array_in_flames;

enum ENUM {
    SET = 1,
    ASK = 2,
};

}  // alice_and_array_in_flames

class alice_and_array_in_flames::alice_and_array_in_flames {
 private:
    std::vector<int> m_integers;

    int set(const std::pair<int, int> &query);
    int ask(const std::pair<int, int> &query);

 public:
    explicit alice_and_array_in_flames(std::vector<int> integers);

    int operate(const int &type, const std::pair<int, int> &query);
};

#endif  // SRC_P3283_P3283_H_
