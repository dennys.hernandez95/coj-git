/* Copyright (c) 2018 Kevin Villalobos */

#include <iostream>
#include <utility>
#include <vector>

namespace tools = alice_and_array_in_flames;

int main() {
    int N, Q;

    std::cin >> N >> Q;
    std::vector<int> integers;

    for (int i = 0; i < N; i++) {
        int val;
        std::cin >> val;
        integers.push_back(val);
    }

    tools::alice_and_array_in_flames tool(integers);

    for (int i = 0; i < Q; i++) {
        int type;
        static std::tuple<int, int> query;

        std::cin >> type >> query.first >> query.second;
        int res = tool.operate(type, query);
        if (type == tools::ASK)
            std::cout << res << std::endl;
    }

    return 0;
}
