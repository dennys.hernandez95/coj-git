# 3323 - Even Number of Divisors

## Description

Given positive integers N and M, find how many integers k are there such that
N <= k <= M and the number of divisors of k is even.

## Input specification

Input consists of several test cases, no more than 1000.
Each case consists of a line with integers N and M (1 <= N <= M <= 10^15)
separated by a single blank space.
Last line of input is followed by a line containing two zeros,
which should not be processed.

## Output specification

For each case you must print the number of integers
between N and M having an even number of divisors in a single line.

## Sample input

```
1 2
1 3
2 5
0 0
```

## Sample output

```
1
2
3
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:

[2156](http://coj.uci.cu/24h/problem.xhtml?pid=2156) |
[3479](http://coj.uci.cu/24h/problem.xhtml?pid=3479) |
[1811](http://coj.uci.cu/24h/problem.xhtml?pid=1811) |
[2141](http://coj.uci.cu/24h/problem.xhtml?pid=2141) |
[1650](http://coj.uci.cu/24h/problem.xhtml?pid=1650) |
[2243](http://coj.uci.cu/24h/problem.xhtml?pid=2243)

## Source

[Even Number of Divisors](http://coj.uci.cu/24h/problem.xhtml?pid=3323)
