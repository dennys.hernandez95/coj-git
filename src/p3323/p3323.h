/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P3323_P3323_H_
#define SRC_P3323_P3323_H_

#include <utility>

namespace even_number_of_divisors {

    // EvenNumbersOfDivisors
    int enod_numbers(const std::pair<int, int> &query);

}

#endif  // SRC_P3323_P3323_H_
