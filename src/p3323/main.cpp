/* Copyright (c) 2018 Kevin Villalobos */

#include <utility>

#include "p3323.h"

namespace tools = even_number_of_divisors;

int main() {
    std::pair<int, int> query;

    while (std::cin >> query.first >> query.second
        && (query.first && query.second) ) {
        std::cout << tools::enod_numbers(query) << std::endl;
    }

    return 0;
}
