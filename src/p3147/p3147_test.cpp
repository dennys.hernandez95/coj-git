/* Copyright (c) 2018 Kevin Villalobos */
#include "p3147.h"

#include <vector>
#include <tuple>

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = sum_challenge;

BOOST_AUTO_TEST_CASE(not_possible,
    *utf::timeout(1)) {
    const std::vector<int> test_vec = {
        -4, -7, 10, 6, 4
    };

    const bool result = tools::zero_sum_subset(test_vec);

    BOOST_REQUIRE_EQUAL(result, false);
}

BOOST_AUTO_TEST_CASE(possible,
    *utf::timeout(1)) {
    const std::vector<int> test_vec = {
        -5, -2, -3, 5, 8
    };

    const bool result = tools::zero_sum_subset(test_vec);

    BOOST_REQUIRE_EQUAL(result, true);
}
