/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P3147_P3147_H_
#define SRC_P3147_P3147_H_

#include <vector>


namespace sum_challenge {

    bool zero_sum_subset(const std::vector<int> &main_set);

}  // namespace sum_challenge

#endif  // SRC_P3147_P3147_H_
