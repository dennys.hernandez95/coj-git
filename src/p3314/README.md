# 3314 - Fast Traveler Game

## Description

Fast Traveler is a board game in which players compete to arrive first at the end of a way.
Let´s define a way as a sequence of C consecutive squares conveniently numbered between 1 and C.
Players are conveniently numbered between 1 and J
and they take turns always in the same order.
First the player number 1, second the player number 2, and so on until the player number J.
On each turn a player throws a dice and moves forward
as many squares as the value of the dice indicates.
Each player begins with their piece in the first square.

Additionally,
all squares have a integer number that represents a compulsory movement
that players must do when falling into that square (only after a dice-throw).
You can safely assume that compulsory movement can always be done
and pieces never go outside the board;
i.e. never going before first or after last square of the board.
Compulsory movement must be done according to the following rules:

* Players must use compulsory movements exactly one time per throw.
* The player must stay in the same square if their piece falls into a square with number 0.
* The player must go backward if their piece falls into a square with a negative value; the piece moves back as many squares as the compulsory movement indicates (absolute value of the number into the square).
* The player must go forward if their piece falls into a square with a positive value; the piece advances as many squares as the compulsory movement indicates (absolute value of the number into the square).

The game reaches the end for players when they fall into the last square of the way;
they don't throw the dice again and they win the game in the order they reach that place/square.
Note that other players could continue playing nonetheless.
It is guaranteed that the last square always has compulsory movement equal to zero.
It is your task to simulate the game process and determine the sequence of numbers
that indicates the players who won the game, on that order (if there is one winner at least).

## Input specification

The first input line has a integer number T (1 <= T <= 100) that represents the number of test cases.
Each test case has 3 lines.
The first has 3 integers:
N (2 <= N <= 10) the number of players,
S (5 <= S <= 100) the number of board squares,
and D (1 <= D <= 1000) the number of dice throws.
The next line has the values of board squares and
the last line has the values of dice throws in the order which should be taken.
Dice throws are always between 0 and 9,
and you can safely assume that moves can always be done and pieces never go outside the board;
i.e. never going after last square of the board.
All throws are valid and it is guaranteed there is at least
one player who is not in the last square on that moment.

## Output specification

For each case output a line with the sequence of numbers of the winners,
in the order they reach the end of the way.
Print all numbers separated by exactly one blank space.
In case of having no winner, print the value -1 instead.

## Sample input

```
2
5 8 12
0 -1 5 -1 0 -2 1 0
3 2 7 1 1 5 3 4 1 3 3 3
3 5 8
0 -1 0 -1 0
1 1 1 1 1 1 1 1
```

## Sample output

```
2 3 1 5 4
-1
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[1000](http://coj.uci.cu/24h/problem.xhtml?pid=1000) |
[1049](http://coj.uci.cu/24h/problem.xhtml?pid=1049) |
[1494](http://coj.uci.cu/24h/problem.xhtml?pid=1494) |
[1028](http://coj.uci.cu/24h/problem.xhtml?pid=1028) |
[3599](http://coj.uci.cu/24h/problem.xhtml?pid=3599) |
[1960](http://coj.uci.cu/24h/problem.xhtml?pid=1960)

## Source

[Fast Traveler Game](http://coj.uci.cu/24h/problem.xhtml?pid=3314)
