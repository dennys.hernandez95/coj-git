/* Copyright (c) 2018 Kevin Villalobos */

#include <vector>

#include "p3314.h"

namespace tools = fast_traveler_game;

int main() {
    int T;
    std::cin >> T;

    while (T--) {
        int N, S, D;

        std::vector<int> squares;
        std::vector<int> dice_vals;

        while (S--) {
            int square;
            std::cin >> square;
            squares.push_back(square);
        }

        while (D--) {
            int dice_val;
            std::cin >> dice_val;
            dice_vals.push_back(dice_val);
        }

        std::vector<int> winners = tools::winners(N, squares, dice_vals);
        for (
            std::vector<int>::iterator it = winners.begin();
            it != winners.end;
            it++) {
            std::cout << (*it) << std::endl;
        }
    }

    return 0;
}
