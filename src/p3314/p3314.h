/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P3314_P3314_H_
#define SRC_P3314_P3314_H_

#include <vector>

namespace fast_traveler_game {

std::vector<int> winners(
    const int &n_players,
    const std::vector<int> &squares,
    const std::vector<int> &dice_vals);

}  // fast_traveler_game

#endif  // SRC_P3314_P3314_H_
