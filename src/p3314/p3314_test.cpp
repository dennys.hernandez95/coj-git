/* Copyright (c) 2018 Kevin Villalobos */
#include "p3314.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = fast_traveler_game;

BOOST_AUTO_TEST_CASE(with_winners,
    *utf::timeout(1)) {
    const int n_players = 5;

    const std::vector<int> squares = {
        0, -1, 5, -1, 0, -2, 1, 0
    };

    const std::vector<int> dice_vals = {
        3, 2, 7, 1, 1, 5, 3, 4, 1, 3, 3, 3
    };

    const std::vector<int> expected_winners = {
        2, 3, 1, 5, 4
    };

    const std::vector<int> winners = tools::winners(
        n_players,
        squares,
        dice_vals);

    BOOST_REQUIRE_EQUAL(expected_winners, winners);
}

BOOST_AUTO_TEST_CASE(without_winners,
    *utf::timeout(1)) {
    const int n_players = 3;

    const std::vector<int> squares = {
        0, -1, 0, -1, 0
    };

    const std::vector<int> dice_vals = {
        1, 1, 1, 1, 1, 1, 1, 1,
    };

    const std::vector<int> expected_winners = {
        -1
    };

    const std::vector<int> winners = tools::winners(
        n_players,
        squares,
        dice_vals);

    BOOST_REQUIRE_EQUAL(expected_winners, winners);
}
