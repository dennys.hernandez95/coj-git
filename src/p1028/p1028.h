/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P1028_P1028_H_
#define SRC_P1028_P1028_H_

#include <iostream>
#include <string>

namespace all_in_all {

    bool is_subsequence(const std::string &s, const std::string &t);

}  // namespace all_in_all

#endif  // SRC_P1028_P1028_H_
