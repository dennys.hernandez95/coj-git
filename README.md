# coj-git

A set of templates to facilitate introduction into programing using boost,
Google’s C++ style guide and problems from the Caribbean Online Judge.

## Requirements

* `g++`
* `boost`
* `python`
* `make`
* `cmake`

## How to use

### What you get

Each folder inside `src\` has the following files:

* `README.md`, a description of the problem.
* `main.cpp`, the main file to the problem.
* `pWXYZ.h`, the headers and *soul* of the solution.
* `pWXYZ_test.cpp`, the test source.

#### `README.md`

This file includes a description of the problem and a link to the
**coj**'s original source.

#### `main.cpp`

A basic implementation to solve the problem.

Here the headers are called and the input is read.
This file doesn't solves the problem (at least not by itself),
but is the one that puts all together.

#### `pWXYZ.h`

The file containing the prototypes of the functions that will be used.
The way it is written is based on my own idea of a *good* way to
build the library needed to solve the problem.

Here should lay the *soul* of the solution.

#### `pWXYZ_test.cpp`

Here lays the tests.
Each one tries to emulate the expected output from the implemented libraries.
For mor info on how this works you could take a look on the
[boost libraries' official site](https://www.boost.org/).

### What you need

Each problem can be solved with the proper implementation of a `.cpp` file
and some little modifications to the `.h` file.

To run the tests you need to have the following files inside the problem's folder:

* `Makefile`
* `CMakeLists.txt`

This files can be created via
`$ make create_base_files`
inside the repository's root.

*For more info use `$ make help` or take a look to the `Makefile`*

After the `.cpp` implementation, the tests can be run using the `Makefile` within
the problem's folder:

* `$ make run` runs the tests within `pWXYZ_test.cpp`.
* `$ make lint` runs the lint test.

*For more info use `$ make help` or take a look to the `Makefile`*

### What now?

After the `.cpp` are uploaded to gitlab, `gitlab-ci` will test them and
display the status of the current commit in the site.

The remaining part is to upload the solution to the [coj](http://coj.uci.cu).

## About the future

The problems will be uploaded as fast as I can.
Any help is appreciated.

Fork this project and start programming!

Happy coding!
